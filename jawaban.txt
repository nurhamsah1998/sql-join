Soal 1 Mengambil Data dari Database

a. Mengambil data users
SELECT id, name, email FROM users;

b. Mengambil data items
SELECT * FROM items WHERE price > 1000000;
SELECT * FROM items WHERE name like '%watch%';

c. Menampilkan data items join dengan kategori
SELECT * FROM `items` LEFT JOIN categories on items.category_id = categories.id;


Soal 2 Mengubah Data dari Database 
UPDATE items SET price=2500000 WHERE name='Sumsang b50';